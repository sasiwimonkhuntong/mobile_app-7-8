package com.example.dialog;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final String[] CLUBS =
            {"Arsenal", "Chelsea", "Liverpool", "Man City", "Man Utd"};
    String mSelected;
    ArrayList<Integer> mMultiSelected;
    private Button btn_dig1, btn_dig2, btn_dig3, btn_dig4, btn_dig5, btn_dig6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_dig1 = (Button) findViewById(R.id.button_dig1);
        btn_dig2 = (Button) findViewById(R.id.button_dig2);
        btn_dig3 = (Button) findViewById(R.id.button_dig3);
        btn_dig4 = (Button) findViewById(R.id.button_dig4);
        btn_dig5 = (Button) findViewById(R.id.button_dig5);
        btn_dig6 = (Button) findViewById(R.id.button_dig6);
        final EditText edtmain = (EditText) findViewById(R.id.editTextMain);
        btn_dig1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dig1 = new
                        AlertDialog.Builder(MainActivity.this);
                dig1.setTitle("Simple Alert Dialog");
                dig1.setMessage("Are you sure,You wanted to make decision ?");
                dig1.setIcon(R.drawable.save);
                dig1.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "You clicked Yes button", Toast.LENGTH_SHORT).show();
                    }
                });
                // if do nothing
                //dig1.setNegativeButton("No", null);
                dig1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "You clicked No button", Toast.LENGTH_SHORT).show();
                        // if exit the app and go to the HOME
// finish();
                    }
                });
                dig1.setNeutralButton("Cancel", new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(getApplicationContext(), "You clicked on Cancel", Toast.LENGTH_SHORT).show();
                                dialog.cancel();
                            }
                        });
                dig1.create();
                dig1.show();
            }
        });
        btn_dig2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dig2 = new
                        AlertDialog.Builder(MainActivity.this);
                dig2.setTitle("Select your Favorite team");
                dig2.setItems(CLUBS, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String selected = CLUBS[which];
                        Toast.makeText(getApplicationContext(), "คุณชอบทมี " +
                                selected, Toast.LENGTH_SHORT).show();
                    }
                });
                dig2.setNegativeButton("ไม่ชอบซักทีม", null);
                dig2.create();
                dig2.show();
            }
        });
        btn_dig3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dig3 = new
                        AlertDialog.Builder(MainActivity.this);
                dig3.setTitle("Select your favorite Team");
                dig3.setSingleChoiceItems(CLUBS, 0, new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mSelected = CLUBS[which];
                                Toast.makeText(getApplicationContext(), "คุณชอบทมี " +
                                        mSelected, Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                dig3.setNegativeButton("ไม่ชอบซักทีม", null);
                dig3.create();
                dig3.show();
            }
        });
        btn_dig4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMultiSelected = new ArrayList<Integer>();
                AlertDialog.Builder dig4 = new
                        AlertDialog.Builder(MainActivity.this);
                dig4.setTitle("Select Favorite Team");
                dig4.setMultiChoiceItems(CLUBS, null, new
                        DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean
                                    isChecked) {
                                if (isChecked) {
                                    mMultiSelected.add(which);
                                } else if (mMultiSelected.contains(which)) {
                                    mMultiSelected.remove(Integer.valueOf(which));
                                }
                            }
                        });
                dig4.setPositiveButton("ยืนยัน", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        StringBuffer buffer = new StringBuffer();
                        for (Integer team : mMultiSelected) {
                            buffer.append(" ");
                            buffer.append(CLUBS[team]);
                        }
                        Toast.makeText(getApplicationContext(), "คุณชอบ" +
                                buffer.toString(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                dig4.setNegativeButton("ไม่ชอบซักทีม", null);
                dig4.create();
                dig4.show();
            }
        });
        btn_dig5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setTitle("Custom Dialog");
                dialog.setContentView(R.layout.custom_dialog);
                TextView txt_dig;
                ImageView img_dig;
                txt_dig = (TextView) dialog.findViewById(R.id.dig_textView1);
                img_dig = (ImageView) dialog.findViewById(R.id.dig_imageView1);
                txt_dig.setText("Custom Dialog");
                img_dig.setImageResource(R.drawable.windows_logo);
                Button btn_dig_ok = (Button) dialog.findViewById(R.id.dig_button1);
                btn_dig_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        btn_dig6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dig6 = new
                        AlertDialog.Builder(MainActivity.this);
                // get input_dialog.xml view
                LayoutInflater inflater = getLayoutInflater();
                View inputview = inflater.inflate(R.layout.input_dialog, null);
                // set input_dialog.xml to be the layout file of the alertdialog builder
                dig6.setView(inputview);
                final EditText edtinput = (EditText)
                        inputview.findViewById(R.id.editTextInput);
                dig6.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // get user input and set it to Main
                        edtmain.setText(edtinput.getText());
                        Toast.makeText(getApplicationContext(), "Input :" +
                                edtinput.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                dig6.setNegativeButton("Cancel", new
                        DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                dig6.create();
                dig6.show();
            }
        });
    }
}
